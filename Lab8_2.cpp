﻿#include <iostream>
#include <cassert>
#include "inc/matrix.hpp"

/*
Возьмите за основу репозиторий mathutils (https://gitlab.com/mishklgpmi/mathutils).
Дополните класс методами для нахождения определителя матрицы размерами 2х2 и 3х3. +
Для других размеров предусмотрите сообщение «Операция не поддерживается». +
Дополните класс методом для нахождения обратной матрицы размером 2х2 и 3х3. +
Для остальных размеров предусмотрите сообщение «Операция не поддерживается». +
Дополните класс методами для нахождения транспонированной матрицы произвольного размера. +
 • • • Методы должны быть шаблонными. +
 • • • На каждый метод или оператор напишите по одному тесту (пример теста есть в main.cpp в репозитории mathutils). +
*/

using mt::math::Vec2i;
using mt::math::Vec2d;
using mt::math::Mat22i;
using mt::math::Mat22d;
using mt::math::Mat33i;
using mt::math::Mat33d;


int main()
{	
	
	std::cout << "--- Test 1 ---\n" << "Determinant" << std::endl;
	{
		 Mat22i A({ { 
			 {1,2},
			 {3,4}
		} });

		 Mat33i B({ { 
			 {1,2,1},
			 {2,0,1},
			 {2,2,4}
		} });

		int Det_A = A.Det();
		int Det_B = B.Det();

		assert(Det_A == -2);
		assert(Det_B == -10);
	}
	std::cout << "Done!" << std::endl;
	
	
	std::cout << "--- Test 2 ---\n" << "Transposed Matrix" << std::endl;
	{
		Mat22i A({ {
			{1,2},
			{3,4}
		} });

		Mat33i B({ {
			{1,2,3},
			{8,6,4},
			{1,3,5}
	   } });

		Mat22i T_A = A.Transposed();
		Mat33i T_B = B.Transposed();

		assert(T_A.get(0, 0) == 1);
		assert(T_A.get(0, 1) == 3);
		assert(T_A.get(1, 0) == 2);
		assert(T_A.get(1, 1) == 4);

		assert(T_B.get(0, 1) == 8);
		assert(T_B.get(1, 0) == 2);
		assert(T_B.get(1, 1) == 6);
		assert(T_B.get(2, 1) == 4);
	}
	std::cout << "Done!" << std::endl;
	
	
	std::cout << "--- Test 3 ---\n" << "Inverse Matrix" << std::endl;
	{
		 Mat22d A({ { 
			{1,2},
			{3,4}
		} });

		Mat33d B({ {
			{1,2,3},
			{8,5,6},
			{7,8,9}
	   } });

		Mat22d Inv_A = A.Inverse();
		Mat33d Inv_B = B.Inverse();

		std::cout << "Inverse A\n" << Inv_A << std::endl;
		std::cout << "Inverse B\n" << Inv_B << std::endl;

	}
	std::cout << "Done!" << std::endl;
	
	return 0;
}